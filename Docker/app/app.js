const express = require('express')
const mysql = require('mysql');

const con = mysql.createConnection({
    host: "mysql-db",
    user: "root",
    password: "complexpassword",
    database: 'Customers'
});

con.connect(function (err) {

if (err) throw err;
 console.log("Connected!");
});

const app = express()

const port = process.env.PORT

app.get('/', (req, res) => res.send('Hello Toto!'))
app.get("/docker", (req, res) => {

  res.send("hello from docker");

});
app.get('/nodemon', (req, res) => res.send('hello from nodemon'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

